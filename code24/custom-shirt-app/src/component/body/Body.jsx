import React from "react";
import Title from "../adbox/Title";

export default class Body extends React.Component {
  render() {
    return (
      <>
        {/* <li> */}
          <Title title={this.props.title} />
          <p>{this.props.p}</p>
          <>
          <a href="/" className="more">{this.props.btnName}</a>
          </>
        {/* </li> */}
      </>
    );
  }
}
