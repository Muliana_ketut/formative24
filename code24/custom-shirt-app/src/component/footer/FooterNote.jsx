import React from "react";

export default class FooterNote extends React.Component {
  render() {
    return (
      <>
        <span id="footnote">
          {" "}
          <a href="/">Custom Shirts</a> &copy; 2021 | All Rights
          Reserved.
        </span>
      </>
    );
  }
}
