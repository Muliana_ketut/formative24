import React from "react";

export default class Medsos extends React.Component {
  render() {
    return (
      <>
        <h3>Follow Us:</h3>
        <a href="/" target="_blank" className="facebook"></a>{" "}
        <a href="/" target="_blank" className="twitter"></a>{" "}
        <a href="/" target="_blank" className="googleplus"></a>
      </>
    );
  }
}
