import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";

export default class Navigation extends React.Component {
  render() {
    return (
      <>
        <div id="navigation">
          <div class="infos">
            <NavLink activeClassName={this.props.activeClassName} exact to="/">
              Cart
            </NavLink>{" "}
            <NavLink activeClassName={this.props.activeClassName} exact to="/">
              0 items
            </NavLink>
          </div>
          <div>
            <NavLink activeClassName={this.props.activeClassName} exact to="/">
              Login
            </NavLink>{" "}
            <NavLink activeClassName={this.props.activeClassName} exact to="/">
              Register
            </NavLink>
          </div>
          <ul id="primary">
            <li className="selected">
              <NavLink
                activeClassName="selected"
                exact
                to="/"
              >
                <span>Home</span>
              </NavLink>
            </li>
            <li>
              <NavLink
                activeClassName="selected"
                exact
                to="/"
              >
                <span>About</span>
              </NavLink>
            </li>
            <li>
              <NavLink
                activeClassName={this.props.activeClassName}
                exact
                to="/"
              >
                <span>Men</span>
              </NavLink>
            </li>
          </ul>
          <ul id="secondary">
            <li>
              <NavLink
                activeClassName={this.props.activeClassName}
                exact
                to="/"
              >
                <span>Women</span>
              </NavLink>
            </li>
            <li>
              <NavLink
                activeClassName={this.props.activeClassName}
                exact
                to="/"
              >
                <span>Blog</span>
              </NavLink>
            </li>
            <li>
              <NavLink
                activeClassName={this.props.activeClassName}
                exact
                to="/"
              >
                <span>Contact</span>
              </NavLink>
            </li>
          </ul>
        </div>
      </>
    );
  }
}
