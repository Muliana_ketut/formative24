import React from "react";
import Image from "../../asset/images/logo.png";

export default class Logo extends React.Component {
  render() {
    return (
      <>
        <a href="/" id="logo">
          <img src={Image} alt="logo"></img>
        </a>
      </>
    );
  }
}
