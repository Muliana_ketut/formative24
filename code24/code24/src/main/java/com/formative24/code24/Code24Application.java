package com.formative24.code24;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code24Application {

	public static void main(String[] args) {
		SpringApplication.run(Code24Application.class, args);
	}

}
