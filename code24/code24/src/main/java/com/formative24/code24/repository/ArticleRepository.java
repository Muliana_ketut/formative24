package com.formative24.code24.repository;

import com.formative24.code24.modal.Article;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    
}
