package com.formative24.code24.controller;

import java.util.List;

import com.formative24.code24.modal.Article;
import com.formative24.code24.modal.Imghome;
import com.formative24.code24.repository.ArticleRepository;
import com.formative24.code24.repository.ImghomeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/list/")
public class GetControllerApp {
    @Autowired
    private ImghomeRepository repoImghome;

    @Autowired
    private ArticleRepository repoArticle;

    @GetMapping("/imghome")
    public List<Imghome> lImghomes() {
        return repoImghome.findAll();
    }
    @GetMapping("/article")
    public List<Article> lArticle() {
        return repoArticle.findAll();
    }
}
